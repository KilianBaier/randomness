import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Settings extends StatefulWidget {
  final String title;

  const Settings({Key? key, required this.title}) : super(key: key);

  @override
  State<Settings> createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  int min = 0;
  int max = 0;
  Map data = {};
  double _currentSliderValueMax = 0;
  double _currentSliderValueMin = 0;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              children: [
                Text('Max: ${_currentSliderValueMax.round()}'),
                SliderTheme(
                    data: SliderThemeData(
                      activeTrackColor: Colors.deepPurple,
                      thumbColor: Colors.purpleAccent,
                      inactiveTrackColor: Colors.purpleAccent,
                      trackHeight: 5,
                    ),
                    child: (Slider(
                      value: _currentSliderValueMax,
                      label: _currentSliderValueMax.round().toString(),
                      onChanged: (double value) {
                        setState(() {
                          _currentSliderValueMax = value;
                        });
                      },
                      min: 0,
                      max: 100,
                      divisions: 10,
                    ))),
              ],
            ),
            Column(
              children: [
                Text('Min: ${_currentSliderValueMin.round()}'),
                SliderTheme(
                  data: SliderThemeData(
                    activeTrackColor: Colors.deepPurple,
                    thumbColor: Colors.purpleAccent,
                    inactiveTrackColor: Colors.purpleAccent,
                    trackHeight: 5,
                  ),
                  child: Slider(
                      value: _currentSliderValueMin,
                      label: _currentSliderValueMin.round().toString(),
                      onChanged: (double value) {
                        setState(() {
                          _currentSliderValueMin = value;
                        });
                      },
                      min: 0,
                      max: 100,
                      divisions: 10),
                )
              ],
            ),
            ElevatedButton(
                onPressed: () {
                  min = _currentSliderValueMin.toInt();
                  max = _currentSliderValueMax.toInt();
                  Navigator.pop(context, {
                    "min": min,
                    "max": max,
                  });
                },
                style: ElevatedButton.styleFrom(
                  primary: Colors.deepPurple,
                ),
                child: Text("Create new random")),
          ],
        ),
      ),
    );
  }
}
