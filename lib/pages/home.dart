import 'dart:math';

import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  final String title;

  const Home({Key? key, required this.title}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int min = 1;

  int max = 6;

  int rand = 0;

  int newRandIntBound(int min, int max) {
    return Random().nextInt(max - min) + min;
  }


  @override
  Widget build(BuildContext context) {
    rand = newRandIntBound(min, max);
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.deepPurple,
          actions: [
            IconButton(
                onPressed: () {
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content: Text("Random number from $min to $max",
                          style: TextStyle(
                              fontSize: 50, color: Colors.blue.shade600))));
                },
                icon: Icon(Icons.info_outline)),
            IconButton(
                onPressed: () async {
                  final returnedData = await Navigator.pushNamed(
                      context, "/settings",
                      arguments: {
                        "min": min,
                        "max": max,
                      });
                  if (returnedData != null) {
                    Map<String, dynamic> data =
                        returnedData as Map<String, dynamic>;
                    setState(() {
                      if (data["min"] > data["max"]) {
                        min = data["max"];
                        max = data["min"];
                      } else {
                        min = data["min"];
                        max = data["max"];
                      }
                      rand = newRandIntBound(min, max);
                    });
                  }
                },
                icon: Icon(Icons.settings))
          ],
          title: Text("-->${widget.title}<--")),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
              child: Text(
            "$rand",
            style: TextStyle(fontSize: 50),
          )),
          ElevatedButton(
              onPressed: () {
                setState(() {
                  rand = newRandIntBound(min, max);
                });
              },
              style: ElevatedButton.styleFrom(
                primary: Colors.deepPurple,
              ),
              child: Text("Create new random")),
        ],
      ),
    );
  }
}
