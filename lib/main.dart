import 'package:flutter/material.dart';

import 'pages/home.dart';
import 'pages/settings.dart';


void main() {
  runApp(const Randomness());
}

class Randomness extends StatelessWidget {
  const Randomness({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Randomness',
      theme: ThemeData(

        primarySwatch: Colors.blue,
      ),

      initialRoute: "/",
      routes: {
        "/": (context) => Home(title: "Randomness"),
        "/settings": (context) => Settings(title: "Settings")
      },
    );
  }
}